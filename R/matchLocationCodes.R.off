
#' Retrieve CAS Fishery Mapping Table
#'
#' The function retrieves the CAS fishery mapping table from an MS Access database.
#'
#' @param loc_filename File name of the location and fishery ID combinations in CSV format
#' @param territory_prefix Limit verification of location codes to those that start with this value
#'
#' @return A list of three data frames
#' @export
#'
#' @importFrom readr read_csv
#' @importFrom dplyr %>% select_all mutate_if as_tibble
#' @importFrom RODBC sqlQuery odbcDriverConnect odbcClose
#'
getCasFisheryTbl <- function(db_filename, stateprov_prefix) {
  if (!requireNamespace("RODBC", quietly = TRUE)) {
    stop("The package 'RODBC' is needed for this function to work -
         and may only work in MS Windows. Please install it.",
         call. = FALSE)
  }

  # reading 32 bit mdb files requires using 32bit R
  driver_name <-
    paste0("Driver={Microsoft Access Driver (*.mdb, *.accdb)};",
           "DBQ=",
           db_filename)

  db_con <- odbcDriverConnect(driver_name)
  fishery_map_df <-
    sqlQuery(db_con,
             paste0("select * ",
                    "from FisheryLookup where StateProvince = '",
                    stateprov_prefix,
                    "'")) %>%
    mutate_if(is.factor, as.character) %>%
    select_all(tolower) %>%
    as_tibble()

  odbcClose(db_con)

  return(fishery_map_df)
}

#' Retrieve CAS Tag Release Table
#'
#' The function retrieves the CAS tag release table from an MS Access database, primarly to
#' identify indicator tag codes and associated stock code
#'
#' @param loc_filename File name of the location and fishery ID combinations in CSV format
#' @param territory_prefix Limit verification of location codes to those that start with this value
#'
#' @return A list of three data frames
#' @export
#'
#' @importFrom readr read_csv
#' @importFrom dplyr %>% select_all
#' @importFrom RODBC sqlQuery odbcDriverConnect odbcClose
#'
getCasTagReleaseTbl <- function(db_filename) {
  if (!requireNamespace("RODBC", quietly = TRUE)) {
    stop("The package 'RODBC' is needed for this function to work -
         and may only work in MS Windows. Please install it.",
         call. = FALSE)
  }

  # reading 32 bit mdb files requires using 32bit R
  driver_name <-
    paste0("Driver={Microsoft Access Driver (*.mdb, *.accdb)};",
           "DBQ=",
           db_filename)

  db_conn <- odbcDriverConnect(driver_name)
  tag_rel_df <-
    sqlQuery(db_conn,
             paste0("select * from WireTagCode")) %>%
    mutate_if(is.factor, as.character) %>%
    select_all(tolower) %>%
    as_tibble()

  odbcClose(db_conn)

  return(tag_rel_df)
}


#' Match CWT to CAS fisheries
#'
#' Validate the combinations of locations codes and fishery IDs from either RMIS
#' files or the DFO MRP system against the CIS fishery mapping table.  This function
#' identifies location codes and fisheries that don't align to exiting CTC fishery
#' definitions.
#'
#' @param loc_filename File name of the location and fishery ID combinations in CSV format
#' @param cas_db_filename File name of the CAS DB Access file name
#' @param stateprov_prefix Limit verification of location codes to those that start with this value
#'
#' @return A list of three data frames
#' @export
#'
#' @importFrom readr read_csv
#' @importFrom dplyr %>% tibble bind_rows bind_cols distinct mutate mutate_at select
#'
matchLocations <- function(loc_filename, cas_db_filename, stateprov_prefix = "2") {

  #Helper Function to append matched locations to the result list
  appendMatchedLoc <- function(result_list,
                               match_type,
                               cas_match_row,
                               location_code) {
    row <-
      tibble(match_type=match_type, loc_code = location_code) %>%
      bind_cols(cas_match_row)
    result_list$mapped_loc_df <- bind_rows(result_list$mapped_loc_df,
                                           row)
    result_list$used_cas_loc_df <- bind_rows(result_list$used_cas_loc,
                                             cas_match_row)

    return(result_list)
  }

  appendUnMatchedLoc <- function(result_list,
                                 unmatch_type,
                                 stock,
                                 location_code) {
    #Helper function to append unmatched locations to the result list
    new_row <- tibble(unmatch_type, stock, location_code)
    result_list$unmapped_loc_df <-
      bind_rows(result_list$unmapped_loc_df, new_row)

    return(result_list)
  }

  cas_fishery_df <-
    getCasFisheryTbl(cas_db_filename, stateprov_prefix) %>%
    select(stock, stateprovince, watertype, sector, region, area, location, sublocation, cwdbfishery) %>%
    distinct() %>%
    mutate(id = 1:nrow(.),
           sector = coalesce(sector, ""),
           watertype = coalesce(watertype, ""))

  cas_tag_df <-
    getCasTagReleaseTbl(cas_db_filename) %>%
    select(tagcode, stock)

  cwt_loc_df <-
    read_csv(loc_filename) %>%
    select_all(tolower) %>%
    inner_join(cas_tag_df, by=c(tag_code = "tagcode")) %>%
    distinct(stock, location_code, fishery_id) %>%
    mutate(stateprovince = as.integer(substring(location_code, 1, 1)),
           water = trimws(substring(location_code,2,2)),
           sector = trimws(substring(location_code,3,3)),
           region = trimws(substring(location_code,4,5)),
           area = trimws(substring(location_code,6,9)),
           loc = trimws(substring(location_code,10,16)),
           subloc = trimws(substring(location_code,17,19)),
           fishery_id = as.integer(fishery_id)) %>%
    mutate_at(c("area", "loc", "subloc"),
              ~ coalesce(as.character(.), ""))


  result_list <-
    list(used_cas_loc_df = NULL,
         unmapped_loc_df = NULL,
         mapped_loc_df = NULL)

  for (row_idx in 1:nrow(cwt_loc_df)) {
    fishery_match <-
      inner_join(cwt_loc_df[row_idx,],
                 cas_fishery_df,
                 by=c("stateprovince",
                      water = "watertype",
                      "sector",
                      fishery_id = "cwdbfishery"))


    reg_match <- filter(fishery_match, region.x == region.y)
    area_match <- filter(reg_match, area.x == area.y)
    loc_match <- filter(area_match, loc == location)
    subloc_match <- filter(loc_match, subloc == sublocation)

    reg_only_match <- filter(reg_match,
                             is.na(area.y) == TRUE,
                             is.na(location) == TRUE,
                             is.na(sublocation) == TRUE,
                             stock.x == stock.y)
    if (nrow(reg_only_match) == 0) {
      reg_only_match <- filter(reg_match,
                               is.na(area.y) == TRUE,
                               is.na(location) == TRUE,
                               is.na(sublocation) == TRUE,
                               is.na(stock.y))
    }

    area_only_match <- filter(area_match,
                              is.na(location) == TRUE,
                              is.na(sublocation) == TRUE,
                              stock.x == stock.y)

    if (nrow(area_only_match) == 0) {
      area_only_match <- filter(area_match,
                                is.na(location) == TRUE,
                                is.na(sublocation) == TRUE,
                                is.na(stock.y))
    }

    loc_only_match <- filter(loc_match,
                             is.na(sublocation) == TRUE)

    if (nrow(loc_only_match) == 0) {
      loc_only_match <- filter(loc_match,
                               is.na(sublocation) == TRUE,
                               is.na(stock.y))
    }

    if (nrow(fishery_match) == 0) {
      appendUnMatchedLoc(result_list,
                         "No Fishery/Water Type/Sector Match",
                         cwt_loc_df$stock[row_idx],
                         cwt_loc_df$location_code[row_idx])
      next
    }

    if (nrow(reg_match) == 0) {
      appendUnMatchedLoc(result_list,
                         "No Region Match",
                         cwt_loc_df$stock[row_idx],
                         cwt_loc_df$location_code[row_idx])
      next
    }

    if (nrow(reg_only_match) == 1 && nrow(area_only_match) == 0) {
      match_desc <- if_else(is.na(reg_only_match$stock.y),
                            "Match Region",
                            "Match Region + Stock")

      result_list <-
        appendMatchedLoc(result_list,
                         match_desc,
                         filter(cas_fishery_df, id == reg_match$id[1]),
                         cwt_loc_df$location_code[row_idx])
    } else  {
      if (nrow(area_match) == 0) {
        result_list <-
          appendUnMatchedLoc(result_list,
                             "No Area Match",
                             cwt_loc_df$stock[row_idx],
                             cwt_loc_df$location_code[row_idx])
      } else if (nrow(area_only_match) == 1 && nrow(loc_only_match) == 0) {
        match_desc <- if_else(is.na(area_only_match$stock.y),
                              "Match Area",
                              "Match Area + Stock")
        result_list <-
          appendMatchedLoc(result_list,
                           match_desc,
                           filter(cas_fishery_df, id==area_match$id[1]),
                           cwt_loc_df$location_code[row_idx])

      } else {
        if (nrow(loc_match) == 0) {
          result_list <-
            appendUnMatchedLoc(result_list,
                               "No Location Match",
                               cwt_loc_df$stock[row_idx],
                               cwt_loc_df$location_code[row_idx])
        } else if (nrow(loc_only_match) == 1 && nrow(subloc_match) == 0) {
          match_desc <- if_else(is.na(loc_only_match$stock.y),
                                "Match Location",
                                "Match Location + Stock")
          result_list <-
            appendMatchedLoc(result_list,
                             "Match Location",
                             filter(cas_fishery_df, id == loc_match$id[1]),
                             cwt_loc_df$location_code[row_idx])
        } else  {
          if (nrow(subloc_match) == 0) {
            result_list <-
              appendUnMatchedLoc(result_list,
                                 "No Sub-Location Match",
                                 cwt_loc_df$stock[row_idx],
                                 cwt_loc_df$location_code[row_idx])
          } else if (nrow(subloc_match) == 1) {
            match_desc <- if_else(is.na(subloc_match$stock.y),
                                  "Match Sub-Location",
                                  "Match Sub-Location + Stock")
            result_list <-
              appendMatchedLoc(result_list,
                               match_desc,
                               filter(cas_fishery_df, id == subloc_match$id[1]),
                               cwt_loc_df$location_code[row_idx])
          } else {
            result_list <-
              appendUnMatchedLoc(result_list,
                                 "No Match",
                                 cwt_loc_df$stock[row_idx],
                                 cwt_loc_df$location_code[row_idx])
          }
        }
      }
    }
  }

  unmapped_filename <- "Unmapped CAS Locations.csv"
  write.csv(result_list$unmapped_loc_df,
            file = unmapped_filename,
            row.names=FALSE,
            na = "")
  cat(paste(coalesce(nrow(result_list$unmapped_loc_df), 0L),
            "unmapped CAS Locations written to:",
            normalizePath(unmapped_filename, mustWork = FALSE),
            "\n"))

  mapped_filename <-  "./Mapped CAS Locations.csv"
  write.csv(result_list$mapped_loc_df,
            file = mapped_filename,
            row.names=FALSE,
            na = "")

  cat(paste(coalesce(nrow(result_list$unmapped_loc_df), 0L),
            "mapped CAS Locations written to:",
            normalizePath(mapped_filename, mustWork = FALSE),
            "\n"))


  result_list$used_cas_loc_df <- distinct(result_list$used_cas_loc_df)
  unused_csas_loc_df <-
    cas_fishery_df %>%
    dplyr::setdiff(result_list$used_cas_loc_df)


  unused_loc_filename <- "./Derilict CAS Locations.csv"
  write.csv(unused_csas_loc_df,
            file = unused_loc_filename,
            row.names=FALSE,
            na = "")
  cat(paste(coalesce(nrow(unused_csas_loc_df), 0L),
            "mapped CAS Locations written to:",
            normalizePath(unused_loc_filename, mustWork = FALSE),
            "\n"))
}
