% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/HRJfunctions.R
\name{compareHRJdbase}
\alias{compareHRJdbase}
\title{Compare two hrj access databases.}
\usage{
compareHRJdbase(
  mdb.filenames,
  tableName = c("HRJ_BY", "HRJ_CY"),
  data.type = "NomCat",
  merge.cols = c("value", "StockID", "fisheryindex", "stocknumber", "broodyear",
    "age.index", "data.type", "age"),
  differenceLimit = 10,
  ...
)
}
\arguments{
\item{mdb.filenames}{A character vector of length 2. The filenames of the MS Access data bases.}

\item{tableName}{A character vector of length 1. The name of the Access table to be compared.}

\item{data.type}{character vector of length 1. The name of the data field to be compared. Default is "NomCat".}

\item{merge.cols}{A character vector. The table field names to base the table merge.}

\item{differenceLimit}{A numeric of length 1. The numeric threshold to evaluate for differences between tables.}

\item{...}{}
}
\value{
A data frame including only table rows with data differences.
}
\description{
Compare two hrj access databases.
}
\examples{
\dontrun{
data.stock <- readStockData('STOCFILE_wcvi.stf')
# reading 32 bit mdb files requires using 32bit R
mdb.filenames <- list.files(pattern = ".mdb$", full.names = TRUE, recursive = TRUE)
bigdiff <- compareHRJdbase(mdb.filenames,data.stock=data.stock)
}
}
