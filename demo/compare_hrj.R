


# comments ----------------------------------------------------------------
#author: Michael Folkes
#demo of how to import one or more hrj files for comparison (eg same stock as produced in two different analysis years)


# function defs -----------------------------------------------------------

require(ctctools)

getHRJfilepaths <- function(x, stocks=NA){
  df <- data.frame(filepath=list.files(x, "HRJ$", full.names = TRUE), stringsAsFactors = F)
  df$stock <- substr(tools::file_path_sans_ext(basename(df$filepath)), 1,3)
  if(any(! is.na(stocks)))  df <- df[df$stock %in% stocks,]
  df
}



# paths and input vars -------------------------------------------------------


dir_year_prior <- "C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/calibration_model2_analyses/2021/data/2021 All HRJ Files"
dir_year_current <- "C:\\Users\\folkesm\\Documents\\Projects\\salmon\\chinook\\ctc\\awg\\calibration_model2_analyses\\2022\\data\\2022 HRJ and Database\\HRJ Database\\All HRJ"



# input and compare -------------------------------------------------------

data.stocks <- read.table(col.names = c("name", "stock", "startage"), sep = ",", text = c("
Atnarko Summer,ATN,2
Big Qualicum,BQR,2
Chilliwack,CHI,2
Cowlitz Fall Tule,CWF,2
Elk River,ELK,2
George Adams Fall Fingerling,GAD,2
Harrison,HAR,2
Kitsumkalum Summer,KLM,3
Columbia Lower River Hatchery,LRH,2
Lewis River Wild,LRW,2
Nicola River Spring,NIC,3
Nisqually Fall Fingerling,NIS,2
Northern SE AK,NSA,3
Puntledge Summer,PPS,2
Queets Fall Fingerling,QUE,2
Quinsam Fall,QUI,2
Robertson Creek,RBT,2
Samish Fall Fingerling,SAM,2
Lower Shuswap River Summers,SHU,2
Skagit Spring Fingerling,SKF,2
Spring Creek Tule,SPR,2
South Puget Sound Fall Fingerling,SPS,2
South Puget Sound Fall Yearling,SPY,2
Salmon River,SRH,2
Southern SE AK,SSA,3
Skagit Summer Fingerling,SSF,2
Columbia Summers,SUM,2
Transboundary Rivers,TST,3
Upriver Brights,URB,2
White River Spring Yearling,WRY,2
Willamette Spring,WSH,3
"))

stocks <- data.stocks$stock

#this will import all hrj's in a folder
#including stocks argument will limit import to erastocks identified

file_year_prior <- getHRJfilepaths(dir_year_prior, stocks)
file_year_current <- getHRJfilepaths(dir_year_current, stocks)

stocks.prior <- unique(file_year_prior$stock)
stocks.current <- unique(file_year_current$stock)

setdiff(stocks.current, stocks.prior)
setdiff(stocks.prior, stocks.current)


hrjdiffs <- compareHRJtext2(file_year_current$filepath, file_year_prior$filepath, differenceLimit = 0)

#rename columns
colnames(hrjdiffs$cwtdiff)[colnames(hrjdiffs$cwtdiff)=="value.x"] <- "value_year_current"
colnames(hrjdiffs$cwtdiff)[colnames(hrjdiffs$cwtdiff)=="value.y"] <- "value_year_prior"
colnames(hrjdiffs$escapementdiff)[colnames(hrjdiffs$escapementdiff)=="value.x"] <- "value_year_current"
colnames(hrjdiffs$escapementdiff)[colnames(hrjdiffs$escapementdiff)=="value.y"] <- "value_year_prior"



# select data type and export ---------------------------------------------
#this outputs all difference data:
eratools::writeXLlistofDF(hrjdiffs[2:3], filename = "hrj_compare_2022vs2021.xls")



nomtot.diffs <- hrjdiffs$cwtdiff[hrjdiffs$cwtdiff$data.type=="NomTot",]
View(nomtot.diffs[order(nomtot.diffs$abs.diff, decreasing = TRUE),])
write.csv(nomtot.diffs, "hrj_nomtot.diffs.csv", row.names = F)

pop.diffs <- hrjdiffs$cwtdiff[hrjdiffs$cwtdiff$data.type=="Pop" & hrjdiffs$cwtdiff$fishery==1,]
View(pop.diffs[order(pop.diffs$abs.diff, decreasing = TRUE),])
write.csv(pop.diffs, "hrj_pop.diffs_fishery1only.csv", row.names = F)

esc.diffs <- hrjdiffs$escapementdiff
unique(esc.diffs$StockID)
View(esc.diffs)
write.csv(esc.diffs, "hrj_esc.diffs.csv", row.names = F)


writeClipboard(knitr::kable(esc.diffs, row.names = F))
writeClipboard(knitr::kable(nomtot.diffs, row.names = F))
