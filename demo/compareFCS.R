

# comments ----------------------------------------------------------------

#author: Michael Folkes

#this compares two fcs files (example is comparing 2021 and 2020)


# function defs -----------------------------------------------------------

require(ctctools)




# data import -------------------------------------------------------------

fcs2020 <- readFCS("C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/calibration_model2_analyses/2020/data/OCN2020_20200328.FCS")

fcs2021 <- readFCS("C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/calibration_model2_analyses/2021/data/OCN2021c.FCS")

str(fcs2020)
names(fcs2020$data.long)
str(fcs2020$data.long)

(names(fcs2020$data.long))

#combine years
data.merge <- merge(fcs2021$data.long, fcs2020$data.long, by=c("stock", "year", "data.type", "age.structure", "agegroup"), all = TRUE)


modelstocks <- c('LGS', 'MGS', 'WVH', 'WVN', 'BON', 'CWF', 'LRW', 'MCB', 'MOC', 'NOC', 'NSA', 'SKG', 'SPR', 'SSA', 'URB', 'WCH', 'WCN', 'WSH', 'TST')

#subset results to just chosen model stocks
data.merge <- data.merge[data.merge$stock %in% modelstocks,]

#rename columns
colnames(data.merge)[colnames(data.merge)=="value.x"] <- "value2021"
colnames(data.merge)[colnames(data.merge)=="value.y"] <- "value2020"

#calcu differerences
data.merge$diff <- data.merge$value2021-data.merge$value2020
data.merge$diff.abs <- abs(data.merge$diff)

#extract out where difference not equal zero
diffs <- data.merge[!is.na(data.merge$diff) & data.merge$diff !=0, ]
nrow(diffs)
View(diffs)

write.csv(diffs, "FCScompare_2020to2021_modelstocks_only.csv", row.names=F)
