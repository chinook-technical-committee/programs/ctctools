

# comments ----------------------------------------------------------------

#give a data frame of file paths to new and prior year fpa files, this compares and updates 9.999 values in new file with value found in previous year.



# packages ----------------------------------------------------------------



require(ctctools)
require(ggplot2)


getFisheryFromFilename <- function(x){
  
  fisheryfiles <- tools::file_path_sans_ext(basename(x))
  fisheryfiles <- gsub("[0-9]*", "", fisheryfiles)
  data.frame(fishery=fisheryfiles, filepath=x, stringsAsFactors = FALSE)
}#END getFisheryFromFilename


getFPAlongtable <- function(filenames){
  filenames.df <- getFisheryFromFilename(filenames)
  
  res <- apply(filenames.df, 1, function(x){
    x <- as.list(x)
    res.temp <- readFPA(x$filepath)
    res.temp[[1]]$dat.long$fishery <- x$fishery
    res.temp[[1]]$dat.long
  })
  res <- do.call('rbind', res)
  
}#END getFPAlongtable

mergeFPAfilepaths <- function(fpa.new.filepaths, fpa.prior.filepaths, fisherynames){
  
  fisherynames <- fisherynames[complete.cases(fisherynames), ]
  fpa.new.filepaths <- merge(fpa.new.filepaths, fisherynames, by.x = "fishery", by.y = "fishery.new")
  fpa.prior.filepaths <- merge(fpa.prior.filepaths, fisherynames, by.x = "fishery", by.y = "fishery.prior")
  merge(fpa.new.filepaths, fpa.prior.filepaths, by.x = 'fishery', by.y = 'fishery.new')
  
}#END mergeFPAfilepaths


setCommonFisheries <- function(fpa.new.filepaths, fpa.prior.filepaths){
  
  fpa.new.filepaths <- getFisheryFromFilename(fpa.new.filepaths)
  fpa.prior.filepaths <- getFisheryFromFilename(fpa.prior.filepaths)
  
  #common names:
  fishery.merge <- data.frame(fishery.new=intersect(fpa.new.filepaths$fishery, fpa.prior.filepaths$fishery))
  fishery.merge$fishery.prior <- fishery.merge$fishery
  
  
  fishery.merge <- rbind(fishery.merge, data.frame(fishery.new=setdiff(fpa.new.filepaths$fishery, fpa.prior.filepaths$fishery), fishery.prior=NA))
  fishery.merge <- rbind(fishery.merge, data.frame(fishery.new=NA, fishery.prior=setdiff(fpa.prior.filepaths$fishery, fpa.new.filepaths$fishery)))
  
  if("GEOStSptb" %in%  fishery.merge$fishery.prior) {
    fishery.merge$fishery.prior[fishery.merge$fishery.prior=="GEOStSptb"] <- NA
    fishery.merge$fishery.prior[fishery.merge$fishery.new=="GSS"] <- "GEOStSptb"
  }
  if("VIisbm" %in%  fishery.merge$fishery.prior) {
    fishery.merge$fishery.prior[fishery.merge$fishery.prior=="VIisbm"] <- NA
    fishery.merge$fishery.prior[fishery.merge$fishery.new=="WCVIISBMS"] <- "VIisbm"
  }
  
  fishery.merge[! (is.na(fishery.merge$fishery.new) & is.na(fishery.merge$fishery.prior)),]
}#END setCommonFisheries

# setup -------------------------------------------------------------------

# revise paths to current and previous year fpa files

setwd("C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/calibration_model2_analyses/2021/R")

fpa.new.filepaths <- list.files(path = "C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/calibration_model2_analyses/2021/data/fpa_test/unedited", pattern = "fpa$",  full.names = TRUE, )

fpa.prior.filepaths <- list.files(path = "C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/calibration_model2_analyses/2020/data/HRJ to FP In_Out for Calib 2002/FPAs used in Calibration 2002", pattern = "fpa$", recursive = TRUE, full.names = TRUE)


fisherynames <- setCommonFisheries(fpa.new.filepaths,fpa.prior.filepaths)
#before proceeding edit fisherynames data frame to fill in any NA values. fisheries with incomplete rows cannot be compared.


fpa.new.filepaths <- getFisheryFromFilename(fpa.new.filepaths)
fpa.prior.filepaths <- getFisheryFromFilename(fpa.prior.filepaths)

fpa.filepaths.merged <- mergeFPAfilepaths(fpa.new.filepaths, fpa.prior.filepaths, fisherynames)

#this data frame identifies what fisheries will be compared
fpa.filepaths.merged



# fpa compare, update, rewrite --------------------------------------------


#this is the way to loop through fpa files and update the newest set of fpa files
res <- apply(fpa.filepaths.merged, 1, function(x){
  
  x <- as.list(x)
  data.new <- readFPA(x$filepath.x)
  data.prior <- readFPA(x$filepath.y)
  data.merge <- merge(data.new[[1]]$dat.long, data.prior[[1]]$dat.long, by=c("model.stocknumber", "age.index", "year"), all.x=TRUE)
  
  #wcvi troll==WCRBT. if stock zero: enter 1.000 in 1985 – 1991, and 1994 – 1999
  if(x$fishery=="WCRBT"){
    data.merge$value.x[data.merge$model.stocknumber==0 & data.merge$year %in% c(1985:1991, 1994:1999)] <- 1
    data.merge$value.x[data.merge$model.stocknumber %in% (13:14) & data.merge$year == 2002] <- 0.005
  }
    
  #if 9.999 replace with prior year value
  data.merge$value.x[data.merge$value.x==9.999] <- data.merge$value.y[data.merge$value.x==9.999]
  res <- data.frame(data.merge[, c("model.stocknumber", "age.index", "year")], value=data.merge$value.x)
  
  #rebuild the first line to match:
  comment.str <- paste(data.new[[1]]$metadata[1:3], collapse = ",")
  filepath <- paste("C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/calibration_model2_analyses/2021/data/fpa_test/Redited",  basename(x$filepath.x), sep="/")
  writeFPA(fpdat = res, filename = filepath,comment = comment.str, year.twodigit = FALSE, projectedyears = 0, yearsep = "   ", sep="  ", nsmall = 3)
})



# plots -------------------------------------------------------------------

fpa.new.filepaths <- list.files(path = "C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/calibration_model2_analyses/2021/data/fpa_test/unedited", pattern = "fpa$",  full.names = TRUE, )

fpa.edited.filepaths <- list.files(path = "C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/calibration_model2_analyses/2021/data/fpa_test/edited", pattern = "fpa$", recursive = TRUE, full.names = TRUE)


fpa.new <- getFPAlongtable(fpa.new.filepaths)
head(fpa.new)
fpa.edited <- getFPAlongtable(fpa.edited.filepaths)
head(fpa.edited)
unique(fpa.new$model.stocknumber)

fpa.altered <- fpa.new[fpa.new$value != fpa.edited$value,]
fpa.altered <- fpa.altered[fpa.altered$value !=9.999,]


p <- ggplot(data = fpa.edited, aes(year, value))+
  ylab("FPA")+
  geom_line()+
  geom_point(data=fpa.altered, aes(year, value) , shape=1, size=0.75, col='red')+
  #facet_grid(cols=vars(model.stocknumber), rows=vars(fishery))+
  facet_grid(fishery~model.stocknumber, scales = "free_y", drop=FALSE)+
  theme(axis.text.x = element_text(angle = 90,hjust = 1, vjust = 0.5, size = 7))
  
#p
ggsave(filename = "fpa_timeseries_canadian.png", plot = p, width = 20, height = 8, units = 'in', dpi=600)


